from django.test import TestCase


class SimpleTest(TestCase):
    def test_basic_addition(self):
        """
        Tests that 1 + 1 always equals 2.
        """
        self.assertEqual(1 + 1, 2)

    def test_basic_subtraction(self):
        """
        Tests that 1 - 1 always equals 0.
        """
        self.assertEqual(1 - 1, 0)

    def test_basic_multiplication(self):
        """
        Tests that 1 * 1 always equals 1.
        """
        self.assertEqual(1 * 1, 1)

    def test_basic_division(self):
        """
        Tests that 1 / 1 always equals 1.
        """
        self.assertEqual(1 / 1, 1)
